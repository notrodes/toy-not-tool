use crate::lexer::Tokens;
use std::sync::Arc;

#[derive(Debug, Clone)]
enum Expressions {
    If,
    Match,
}

#[derive(Debug, Clone)]
enum Data {
    Bool(bool),
    String(String),
    I32(i32),
    F64(f64),
}

#[derive(Debug, Clone)]
enum Values {
    Expressions(Expressions),
    Data(Data),
    Loop,
    Operator,
    Variable(String, Data),
}

#[derive(Debug, Clone)]
struct ID {
    id: u64,
}

impl ID {
    fn new(id: u64) -> ID {
        ID { id }
    }
}

#[derive(Debug, Clone)]
pub struct Ast {
    id: ID,
    children: Vec<Ast>,
    values: Values,
}

impl<'a> Ast {
    fn new(id: ID, children: Vec<Ast>, values: Values) -> Ast {
        Ast {
            id,
            children,
            values,
        }
    }
}

pub fn parse<'a>(tokens: Vec<Tokens>) -> Result<Ast, &'a str> {
    let mut root = Ast::new(
        ID::new(1),
        Vec::new(),
        Values::Data(Data::String("root".to_string())),
    );
    let mut new_id = 2;
    let mut token_iter = tokens.iter();
    let mut scope = &root.id;
    for token in token_iter.clone() {
        match token {
            Tokens::Let => {
                if let Some(Tokens::Identifier(ident_name)) = token_iter.next() {
                    if let Some(Tokens::Equals) = token_iter.next() {
                        root.children.push(Ast::new(
                            ID::new(new_id),
                            Vec::new(),
                            Values::Variable(ident_name.to_string(), Data::Bool(true)),
                        ));
                        new_id += 1;
                    } else {
                        return Err("Syntax err no = after let")
                    }
                }
            }
            _ => {}
        }
    }
    Ok(root)
}
