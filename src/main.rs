use clap::clap_app;
use language::compile;
use std::fs::read_to_string;

fn main() {
    let matches = clap_app!(language =>
    (version: "0.0.0")
        (author: "notrodes")
        (about: "toy not a tool")
        (@subcommand build =>
            (about: "build file into executable")
            (@arg SOURCE: +required "Sets the input file to use")
        )
    )
    .get_matches();
    match matches.subcommand() {
        ("build", Some(file)) => match file.value_of("SOURCE") {
            Some(path) => compile(read_to_string(path).unwrap()),
            None => panic!(),
        },
        (_, None) => panic!(),
        (_, _) => panic!(),
    }
}
