use std::fs;

struct Executable {

}

impl Executable {
    fn new() -> Executable{
        Executable { }
    }

    fn assemble() {

    }
}

mod test {
    #[test]
    fn test() {
        let test_elf = Executable::new();
        test_elf.assemble();
    }
}