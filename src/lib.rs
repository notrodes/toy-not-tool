use crate::lexer::{lexer};
use crate::ast::parse;

mod ast;
mod lexer;

pub fn compile(source: String) {
    println!("{}\n", source);
    let token_stream = lexer(source);
    println!("{:?}\n", token_stream);
    let ast = parse(token_stream);
    println!("{:?}", ast);
}
