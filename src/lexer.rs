#[derive(PartialEq, Debug)]
pub enum Tokens {
    Let,
    Identifier(String),
    Equals,
    Semicolon,
    Value(String),
    OpenCurly,
    CloseCurly,
}

pub fn lexer(source: String) -> Vec<Tokens> {
    let mut token_stream = Vec::new();
    let mut current_token = String::new();
    for char in source.chars() {
        current_token.push(char);
        // let == a var token
        match token_stream.last() {
            Some(token) => {
                if current_token.len() > 1 && char == ' ' || char == ';' {
                    match *token {
                        Tokens::Let => {
                            token_stream.push(Tokens::Identifier(current_token.trim().to_string()));
                            current_token.clear()
                        }
                        Tokens::Equals => {
                            token_stream.push(Tokens::Value(
                                current_token
                                    .clone()
                                    .trim()
                                    .chars()
                                    .take(current_token.len() - 2)
                                    .collect(),
                            ));
                            current_token.clear()
                        }
                        _ => (),
                    }
                }
            }
            None => (),
        }
        if current_token.trim() == "let" {
            token_stream.push(Tokens::Let);
            current_token.clear()
        } else if char == '=' {
            token_stream.push(Tokens::Equals);
            current_token.clear()
        } else if char == ';' {
            token_stream.push(Tokens::Semicolon);
            current_token.clear()
        } else if char == '{' {
            token_stream.push(Tokens::OpenCurly);
            current_token.clear()
        } else if char == '}' {
            token_stream.push(Tokens::CloseCurly);
            current_token.clear()
        }
    }
    token_stream
}
